#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "myassert.h"

#include "master_client.h"
#include "master_worker.h"

// Bibliothéques ajoutés
#include <unistd.h>     // Pour: fork, pipe
#include <sys/types.h>  // Pour: wait, O_RDONLY, O_WRONLY
#include <sys/wait.h>   // Pour: wait
#include <sys/stat.h> // Pour: O_RDONLY, O_WRONLY
#include <fcntl.h> // Pour: O_RDONLY, O_WRONLY

/************************************************************************
 * Données persistantes d'un master
 ************************************************************************/

// Définition de la structure des données pour le master
typedef struct
{
    int semaphores[2];
    const char* named_tubes[NB_NAMED_PIPES];
    int master_to_worker[SIZE_PIPE];
    int worker_to_master[SIZE_PIPE];
    int writing_master;
    int reading_master;
    int how_many;
    int highest;
} master_data;


/************************************************************************
 * Usage et analyse des arguments passés en ligne de commande
 ************************************************************************/

static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usage : %s\n", exeName);
    if (message != NULL)
        fprintf(stderr, "message : %s\n", message);
    exit(EXIT_FAILURE);
}


/************************************************************************
 * boucle principale de communication avec le client
 ************************************************************************/

void loop(master_data data)
{
    // Boucle infinie
    while (true)
    {
        printf("MASTER : En attente d'une instruction d'un client...\n");

        // Ouverture des tubes:
            // Ouverture du tube nommé client vers master en lecture
        int fd_client_master = open_named_pipe(data.named_tubes[PIPE_CLIENT_MASTER], O_RDONLY);
            // Ouverture du tube nommé master vers client en écriture
        int fd_master_client = open_named_pipe(data.named_tubes[PIPE_MASTER_CLIENT], O_WRONLY);

        // Le master recoit l'ordre donné par le client
        int order = receive_data(fd_client_master);

        // Si l'ordre indique de calculer un nombre premier
        if (order == ORDER_COMPUTE_PRIME)
        {
            // On récupère le nombre premier à calculer
            int compute_prime = receive_data(fd_client_master);
            // Avant d'envoyer le nombre dans la pipeline on doit s'assurrer d'envoyer tous les
            // nombres compris entre le nombre à calculer et le plus grand nombre trouvé
            if (compute_prime > data.highest)
            {
                // On envoie les nombres un par un et on regarde si il y a un nouveau nombre premier qui a été trouvé
                for (int i = (data.highest + 1); i < compute_prime; i++)
                {
                    send_data(data.writing_master, i);
                    int result = receive_data(data.reading_master);
                    // Si le nombre parcouru est premier il est alors le plus grand nombre premier
                    if (result == NUMBER_IS_PRIME || result > 1)
                    {
                        data.how_many++;
                        data.highest = i;
                    }
                }
            }
            

            // On envoie le nombre à tester au premier worker
            sent_data(data.writing_master, compute_prime);

            // On regarde les résultat que nous a envoyé un worker
            int result = receive_data(data.reading_master);
            printf("le result recu par le master est%d\n", result);
            if (result > 1)
            {
                result = receive_data(data.reading_master);
            }
            // Si le nombre est premier on l'indique au client
            if (result == NUMBER_IS_PRIME)
            {
                sent_data(fd_master_client, NUMBER_IS_PRIME);
                // Si le nombre testé est plus grand que le plus grand nombre 
                // premier stocké dans la mémoire on le remplace
                if (compute_prime > data.highest)
                {
                    data.how_many++;
                    data.highest = compute_prime;
                }
            }
            // Si il n'est pas premier on l'indique aussi au client
            else if (result == NUMBER_NOT_PRIME)
            {
                sent_data(fd_master_client, NUMBER_NOT_PRIME);
            }    
        }
        else if (order == ORDER_HOW_MANY_PRIME)
            sent_data(fd_master_client, data.how_many);      // Envoi du nombre de nombre premier calculés
        else if (order == ORDER_HIGHEST_PRIME)
            sent_data(fd_master_client, data.highest);  // Envoi du plus grand nombre premier
        else if (order == ORDER_STOP)
        {
            // On envoie l'ordre d'arret au premier worker
            sent_data(data.writing_master, STOP);
            // On attend la fin du premier worker
            wait(NULL);
            sent_data(fd_master_client, ORDER_STOP);               // Envoi d'un accusé de réception au client
        }
        else // Ne doit normalement jamais aller ici sinon il y a une erreur dans le programme
        {
            fprintf(stderr, "L'ordre du client recu par le master est inconnu\n");
            //break;
        }

        // Fermeture des tubes
            // Fermeture du tube nommé client vers master
        close_named_pipe(fd_client_master);
            // Fermeture du tube nommé master vers client
        close_named_pipe(fd_master_client);

        // On diminue le sémaphore entre le master et le client pour attendre la fin du client
        operation_semaphore(data.semaphores[SEM_MASTER_CLIENT], SEM_OP_DEC);

        // Si l'ordre est de stoper le master on sort de la boucle
        if (order == ORDER_STOP)
            break;
    }
}

/************************************************************************
 * Initialisation des données
 ************************************************************************/

master_data init_data()
{
    // Déclaration de la structure qui stocke les données utiles au master
    master_data data;

    // - Création des sémaphores:
        // Création du sémaphore entre les clients
    data.semaphores[SEM_CLIENTS] = creation_semaphore(get_key_semaphore(SEM_ID_CLIENTS), 1);
        // Création du sémaphore entre le master et un client
    data.semaphores[SEM_MASTER_CLIENT] = creation_semaphore(get_key_semaphore(SEM_ID_MASTER_CLIENT), 1);
    printf("Les sémaphores se sont créés et initialisé correctement\n");

    // - Création des tubes nommés
        // Création du tube nommé client vers master
    data.named_tubes[PIPE_CLIENT_MASTER] = create_named_pipe(NAMED_PIPE_CLIENT_MASTER);
        // Création du tube nommé master vers client
    data.named_tubes[PIPE_MASTER_CLIENT] = create_named_pipe(NAMED_PIPE_MASTER_CLIENT);
    printf("Les tubes nommés se sont créés correctement\n");

    // Initialisation du nombre de calcul fait et du plus grand nombre premier trouvé à 0
    data.how_many = 0;
    data.highest = 0;
    
    // Création des deux premiers tubes anonymes
    int ret;
        // Tube anonyme du master vers le worker
    ret = pipe(data.master_to_worker);
    myassert(ret != -1, "Le tube anonyme Master->Worker s'est mal créé");
        // Tube anonyme du worker vers le master
    ret = pipe(data.worker_to_master);
    myassert(ret != -1, "Le tube anonyme Worker->Master s'est mal créé");
    printf("Les tubes anonymes se sont créés correctement\n");

    return data;
}

/************************************************************************
 * Destruction des données
 ************************************************************************/

void destroy_data(master_data data)
{
    sleep(1);

     // - Destruction des tubes nommés:
        // Destruction du tube nommé client vers master
    destroy_named_pipe(data.named_tubes[PIPE_CLIENT_MASTER]);
        // Destruction du tube nommé master vers client 
    destroy_named_pipe(data.named_tubes[PIPE_MASTER_CLIENT]);
    printf("Les tubes nommés se sont détruit correctement\n");



    // - Destruction des sémaphores:
        // Destruction du sémaphore entre les clients
    destroy_semaphore(data.semaphores[SEM_CLIENTS]);
        // Destruction du sémaphore entre le master et un client
    destroy_semaphore(data.semaphores[SEM_MASTER_CLIENT]);
    printf("Les sémaphores se sont détruit correctement\n");

   
}

/************************************************************************
 * Fonction principale
 ************************************************************************/

int main(int argc, char * argv[])
{
    if (argc != 1)
        usage(argv[0], NULL);

    // On initialise les données
    master_data data = init_data();

    // Premier nombre premier à passer au premier worker
    int first_prime_number = 2;
    
    // Création d'un nouveau processus pour executer le premier worker 
    pid_t ret_fork = fork();
    myassert(ret_fork != -1, "Le fork du master pour créer le premier worker s'est mal exécuté");

    // Processus fils issus du fork
    if (ret_fork == 0)
    {
        // On initialise les files descriptors des tubes anonymes dont le premier worker a besoin
        data.reading_master = reading_side_pipe(data.master_to_worker);
        data.writing_master = writing_side_pipe(data.worker_to_master);
        // On créé le premier worker avec le premier nombre premier (ici 2)
        create_worker(first_prime_number, data.reading_master, data.writing_master); // Exec donc le fils s'arrête ici
    }

    // Processus père issus du fork
    // On initialise les files descriptors des tubes anonymes dont le master a besoin
    data.writing_master = writing_side_pipe(data.master_to_worker);
    data.reading_master = reading_side_pipe(data.worker_to_master);
    // On envoie au premier worker le le premier nombre premier (ici 2)
    sent_data(data.writing_master, first_prime_number);
    // On vérifie que le premier worker a reconnu 2 comme étant un nombre premier
    int is_prime = receive_data(data.reading_master);
    if ((is_prime == NUMBER_IS_PRIME) || is_prime > 1 )
    {
        // L'initialisation du premier worker est faite
        printf("Le premier worker a bien reconnu %d comme étant un nombre premier\n", first_prime_number);
        printf("Vous pouvez maintenant lancer un client pour tester d'autres nombres premier\n");
        data.how_many++;
        data.highest = first_prime_number;
    }
    else // Si le programme vient ici c'est qu'il y a eu un problème lors de la création du premier worker
    {
        fprintf(stderr, "La création du premier worker ne s'est pas bien déroulée\n");
        // Destruction des données
        destroy_data(data);
        return EXIT_FAILURE;
    }

    // Boucle infinie pour la communication avec le client
    loop(data);

    // Destruction des données
    
    destroy_data(data);

    return EXIT_SUCCESS;
}