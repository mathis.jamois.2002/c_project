#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "myassert.h"

#include "master_client.h"

// Bibliothéques ajoutés

#include <math.h>     // Pour: sqrt
#include <pthread.h>  // Pour: pthread_t, pthread_create, pthread_join
#include <sys/types.h> // Pour: O_RDONLY, O_WRONLY
#include <sys/stat.h> // Pour: O_RDONLY, O_WRONLY
#include <fcntl.h> // Pour: O_RDONLY, O_WRONLY

// chaines possibles pour le premier paramètre de la ligne de commande
#define TK_STOP      "stop"
#define TK_COMPUTE   "compute"
#define TK_HOW_MANY  "howmany"
#define TK_HIGHEST   "highest"
#define TK_LOCAL     "local"

/************************************************************************
 * Usage et analyse des arguments passés en ligne de commande
 ************************************************************************/

static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usage : %s <ordre> [<number>]\n", exeName);
    fprintf(stderr, "   ordre \"" TK_STOP  "\" : arrêt master\n");
    fprintf(stderr, "   ordre \"" TK_COMPUTE  "\" : calcul de nombre premier\n");
    fprintf(stderr, "                       <nombre> doit être fourni\n");
    fprintf(stderr, "   ordre \"" TK_HOW_MANY "\" : combien de nombres premiers calculés\n");
    fprintf(stderr, "   ordre \"" TK_HIGHEST "\" : quel est le plus grand nombre premier calculé\n");
    fprintf(stderr, "   ordre \"" TK_LOCAL  "\" : calcul de nombre premier en local\n");
    if (message != NULL)
        fprintf(stderr, "message : %s\n", message);
    exit(EXIT_FAILURE);
}

static int parseArgs(int argc, char * argv[], int *number)
{
    int order = ORDER_NONE;

    if ((argc != 2) && (argc != 3))
        usage(argv[0], "Nombre d'arguments incorrect");

    if (strcmp(argv[1], TK_STOP) == 0)
        order = ORDER_STOP;
    else if (strcmp(argv[1], TK_COMPUTE) == 0)
        order = ORDER_COMPUTE_PRIME;
    else if (strcmp(argv[1], TK_HOW_MANY) == 0)
        order = ORDER_HOW_MANY_PRIME;
    else if (strcmp(argv[1], TK_HIGHEST) == 0)
        order = ORDER_HIGHEST_PRIME;
    else if (strcmp(argv[1], TK_LOCAL) == 0)
        order = ORDER_COMPUTE_PRIME_LOCAL;
    
    if (order == ORDER_NONE)
        usage(argv[0], "ordre incorrect");
    if ((order == ORDER_STOP) && (argc != 2))
        usage(argv[0], TK_STOP" : il ne faut pas de second argument");
    if ((order == ORDER_COMPUTE_PRIME) && (argc != 3))
        usage(argv[0], TK_COMPUTE " : il faut le second argument");
    if ((order == ORDER_HOW_MANY_PRIME) && (argc != 2))
        usage(argv[0], TK_HOW_MANY" : il ne faut pas de second argument");
    if ((order == ORDER_HIGHEST_PRIME) && (argc != 2))
        usage(argv[0], TK_HIGHEST " : il ne faut pas de second argument");
    if ((order == ORDER_COMPUTE_PRIME_LOCAL) && (argc != 3))
        usage(argv[0], TK_LOCAL " : il faut le second argument");
    if ((order == ORDER_COMPUTE_PRIME) || (order == ORDER_COMPUTE_PRIME_LOCAL))
    {
        *number = strtol(argv[2], NULL, 10);
        if (*number < 2)
             usage(argv[0], "le nombre doit être >= 2");
    }       
    
    return order;
}

/************************************************************************
 * Fonction locale pour le calcul d'un nombre premier
 ************************************************************************/

// Paramètre pour les threads
typedef struct {
    int value;
    bool is_prime;
    int n;
    pthread_mutex_t *mutex;
} thread_data;

void* code_thread(void *arg)
{
    thread_data* data = (thread_data*)arg;
    int ret;

    for(int i = 2; i*data->value < data->n; i++)
    {
        ret = pthread_mutex_lock(data->mutex);
        myassert(ret == 0, "error: lock du mutex");
        (data+i*data->value)->is_prime = false;
        ret = pthread_mutex_unlock(data->mutex);
        myassert(ret == 0, "error: unlock du mutex");
    }
    
}

thread_data* init_thread(thread_data* donnes, pthread_mutex_t* mutes, int n)
{
    donnes =  malloc(sizeof(thread_data)*(n-1));
    int count = 2;
    for(int i = 0; i < n - 1; i++)
    {
        donnes[i].value = count;
        donnes[i].is_prime = true;
        donnes[i].n = n;
        donnes[i].mutex = mutes;
        count++;
    }
    return donnes;
}

void compute_local_prime(int n)
{
    // mutex déclaré en local
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    
    // nombre de threads à lancer
    int nb_threads = (int)sqrt(n) - 1;

    // On déclare ici un tableau de pthread_t pour stocker tous les
    // identifiants des threads
    pthread_t id_tab[nb_threads];

    // et un tableau de struct pour que chaque thread ait sa propre structure
    thread_data* datas = NULL;
    datas = init_thread(datas, &mutex, n);
    // initialisation des datas[i]

    // lancement des threads
    for(int i = 0; i < nb_threads; i++)
    {
        int ret = pthread_create(&(id_tab[i]), NULL, code_thread, &(datas[i]));
        myassert(ret == 0, "error: creation d'un thread");
    }

    // attente de la fin des threads
    for (int i = 0; i < nb_threads; i++)
    {
        int ret = pthread_join(id_tab[i], NULL);
        myassert(ret == 0, "error: attente de la fin d'un thread de client bis");
    }

    //Destruction du mutex...
    int ret = pthread_mutex_destroy(&mutex);
    myassert(ret == 0, "error: destruction du mutex de client bis");

    // Affichage du résultat
    for(int i = 0; i < n - 1; i++)
    {
        if (datas[i].is_prime == true)
        {
            printf("%d ", datas[i].value);
        }
    }
}


/************************************************************************
 * Fonction principale
 ************************************************************************/

int main(int argc, char * argv[])
{
    int number = 0;
    int order = parseArgs(argc, argv, &number);

    // Si l'ordre est de calculer localement
    // on utilise la fonction avec les threads ci-dessus
    if (order == ORDER_COMPUTE_PRIME_LOCAL)
    {
        compute_local_prime(number);
    }
    else // Sinon il faut communiquer avec le master
    {
        // On prend le sémaphore qui gére les relations entre les clients
        int id_sem_clients = get_id_semaphore(get_key_semaphore(SEM_ID_CLIENTS));
        // On diminue ce sémaphore pour passer en section critique
        operation_semaphore(id_sem_clients, SEM_OP_DEC);

        // Ouverture des tubes
        // Ouverture du tube nommé client vers master en écriture
        int fd_client_to_master = open_named_pipe(NAMED_PIPE_CLIENT_MASTER, O_WRONLY);
        // Ouverture du tube nommé master vers client en lecture
        int fd_master_to_client = open_named_pipe(NAMED_PIPE_MASTER_CLIENT, O_RDONLY);

        // Envoie de l'ordre sur le tube nommé client vers master
        send_data(fd_client_to_master, order);

        // Si le client doit transmettre le nombre premier à calculer
        if (order == ORDER_COMPUTE_PRIME)
        {
            send_data(fd_client_to_master, number); // Envoi du nombre à tester
            client_is_prime(fd_master_to_client, number);   // Réception qui indique si le nombre est premire
        }
        else if (order == ORDER_HOW_MANY_PRIME)
            client_how_many(fd_master_to_client);
        else if (order == ORDER_HIGHEST_PRIME)
            client_highest_prime(fd_master_to_client);
        else if (order == ORDER_STOP)
            client_stop(fd_master_to_client);
        else // Ne doit normalement jamais aller ici sinon il y a une erreur dans le programme
        {
            fprintf(stderr, "L'ordre du client est inconnu\n");
            return EXIT_FAILURE;
        }

        // On augmente le sémaphore entre les clients pour sortir de la section critique
        operation_semaphore(id_sem_clients, SEM_OP_INC);

        // Fermeture des tubes
            // Fermeture du tube nommé client vers master
        close_named_pipe(fd_client_to_master);
            // Fermeture du tube nommé master vers client
        close_named_pipe(fd_master_to_client);

        // On prend le sémaphore qui gére les relations entre le master et un client
        int id_sem_master_and_client = get_id_semaphore(get_key_semaphore(SEM_ID_MASTER_CLIENT));
        // On augmente ce sémaphore pour débloquer le master
        operation_semaphore(id_sem_master_and_client,SEM_OP_INC);
    }
    return EXIT_SUCCESS;
}
