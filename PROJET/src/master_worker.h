#ifndef MASTER_WORKER_H
#define MASTER_WORKER_H

// On peut mettre ici des éléments propres au couple master/worker :
//    - des constantes pour rendre plus lisible les comunications
//    - des fonctions communes (écriture dans un tube, ...)

// Taille du tableau de fd des tubes anonymes
#define SIZE_PIPE 2

// Ordre d'arrêt pour les workers
#define STOP -1

// Pour dire qu'il n'y a pas de suivant
#define NO_NEXT -1

// Pour savoir si un nombre est premier
    // Le nombre n'est pas premier
#define NUMBER_NOT_PRIME 0
    // Le nombre est premier
#define NUMBER_IS_PRIME 1


// Fonctions pour se mettre à un bout ou un autre d'une pipe anonyme :
    // Fournit le côté lecture d'une pipe entrée en paramètre
int reading_side_pipe(int pipe[2]);
    // Fournit le côté écriture d'une pipe entrée en paramètre
int writing_side_pipe(int pipe[2]);

// Ferme les file descriptor entrés en paramètre
void close_fd(int fd_prev, int fd_next, int fd_master);

// Fonctions général d'envoie et réception de données par un tube
void sent_data(int fd, int data);
int receive_data(int fd);

// Pour créer un worker
void create_worker(int prime_number, int previous_pipe, int master_pipe);

#endif
