#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#define _XOPEN_SOURCE

#include <stdlib.h>
#include <stdio.h>

#include "myassert.h"

#include "master_client.h"

// Bibliothéques ajoutés

#include <sys/types.h>  // Pour: ftok, semget, semctl, mkfifo, open, semop
#include <sys/ipc.h>    // Pour: ftok, semget, semctl, semop
#include <sys/sem.h>    // Pour: semget, semctl, semop
#include <sys/stat.h>   // Pour: mkfifo, open
#include <fcntl.h>      // Pour: open
#include <unistd.h>     // Pour: unlink, write, read, close


/**********************************************
            Pour les tubes nommés
 **********************************************/

//============ MANIPULATIONS DE TUBES NOMMES ============

// **** CREATION ****

// Fonction générale pour créé un tube nommé
const char* create_named_pipe(const char* pipe_path_name)
{
    // Crée le tube nommé en lecture/ecriture et teste s'il est bien créé 
    int pipe = mkfifo(pipe_path_name, 0641);
    myassert(pipe != -1, "Un tube nommé s'est mal créé");

    // Retourne le nom du tube s'il est bien créé
    return pipe_path_name;
}

// **** OUVERTURE ****

// Fonction générale pour ouvrir un tube nommé
int open_named_pipe(const char* pipe, int flag)
{
    // Ouvre le tube nommé en lecture et teste s'il a bien été ouvert
    int fd = open(pipe, flag);
    myassert(fd != -1, "L'ouverture d'un tube nommé s'est mal exécutée");

    // Retourne le file descriptor
    return fd;
}

// **** FERMETURE **** 

// Fermeture du tube en paramètre
void close_named_pipe(int fd)
{
    // Ferme le tube et teste s'il a bien été fermé
    int ret = close(fd);
    myassert(ret != -1, "La fermeture du tube a échoué");
}

// **** DESTRUCTION ****

// Détruit le tube nommé dont le nom est passé en paramètre
void destroy_named_pipe(const char* name) 
{
    // Détruit le tube et teste s'il a bien été détruit
    int destroy = unlink(name);
    myassert(destroy != 1, "La destruction de tube nomée s'est mal effectué");
}

//============ UTILISATION DE TUBES NOMMES ============

// Fonction général d'envoie d'une données par un tube nommé
void send_data(int fd, int data)
{
    // Envoie la donnée par le tube mis en paramètre
    int ret = write(fd, &data, sizeof(int));
    myassert(ret != -1, "L'envoie d'une donnée par un tube ne s'est pas bien déroulé");
}

// Fonction général de reception d'une données par un tube nommé
int received_data(int fd)
{
    // Lecture de la donnée par le tube mis en paramètre
    int data;
    int ret = read(fd, &data, sizeof(int));
    myassert(ret != -1, "La lecture d'une donnée par un tube a échoué");

    // Retourne la donnée qui vient d'être lu
    return data;
}

// **** Master->Client : ORDER_HOW_MANY_PRIME ****

// Le client reçoit du master combien de nombre premier ont été calculés
void client_how_many(int fd)
{
    int how_many = received_data(fd);
    printf("Il y a %d nombres premiers calculés\n", how_many);
}

// **** Master->Client : ORDER_HIGHEST_PRIME ****

// Le client reçoit du master le plus grand nombre premier qui ait été calculé
void client_highest_prime(int fd)
{
    int highest_prime = received_data(fd);
    printf("Le plus grand nombre premier calculé est %d\n", highest_prime);
}

// **** Master->Client : ORDER_STOP ****

// Le client reçoit du master un accusé de reception
void client_stop(int fd)
{
    int confirm_receipt = received_data(fd);
    printf("L'accusé de reception %d de l'arrêt du master a bien été reçu\n", confirm_receipt);
}

// **** Master->Client : ORDER_COMPUTE_PRIME ****

// Le client recoit la réponse si le nombre est premier ou pas
void client_is_prime(int fd, int number_tested)
{
    int is_prime = received_data(fd);
    printf("is_prime = %d, fct client_is_prime\n", is_prime);

    if (is_prime == NUMBER_IS_PRIME)
        printf("Le nombre %d est premier\n", number_tested);
    else if (is_prime == NUMBER_NOT_PRIME)
        printf("Le nombre %d n'est pas premier\n", number_tested);
    else // Ne devrait jamais venir ici sinon bug en amont
        fprintf(stderr, "Le nombre %d n'a pas réussi à être testé\n", number_tested);
}


/**********************************************
            Pour les sémaphores
 **********************************************/

//============ MANIPULATIONS DES SEMAPHORES ============

// **** CLE ****

// Fonction générale pour génerer la clé d'un sémaphore
key_t get_key_semaphore(int sem_id_num)
{
    // Génére la clé et test si il n'y a pas eu d'erreur
    key_t key = ftok(SEM_FICHIER, sem_id_num);
    myassert(key != -1, "La clé d'un sémaphore s'est mal générée");

    // Retourne la clé si celle si s'est bien générée
    return key;
}

// **** CREATION ****

// Fonction générale pour créer et initialiser un sémaphore
int creation_semaphore(key_t key, int semValInit)
{
    // Créé le sémpahore et vérifie que tout s'est bien passé
    int sem_id = semget(key, 1, IPC_CREAT | IPC_EXCL | 0641);
    myassert(sem_id != -1, "Un sémaphore ne s'est pas créé correctement");

    // Initialise le sémaphore et vérifie qu'aucun problème ne s'est passé
    int ret = semctl(sem_id, 0, SETVAL, semValInit);
    myassert(ret != -1, "Le sémaphore ne s'est pas correctement initialisé");

    return sem_id;
}

// **** RECUPERATION ****

// Fonction générale pour récupérer un sémaphore existant
int get_id_semaphore(key_t key)
{
    // Récupére le sémpahore et vérifie que tout s'est bien passé
    int sem_id = semget(key, 1, 0);
    myassert(sem_id != -1, "L'identifiant d'un sémaphore ne s'est pas récupéré correctement");

    // Retourne l'identifiant si celui si a bien été généré
    return sem_id;
}

// **** DESTRUCTION ****

// Détruit le sémaphore dont l'identifiant a été passé en paramètre
void destroy_semaphore(int sem_id)
{
    // Détruit le sémaphore et vérifie qu'aucun problème ne s'est passé
    int ret = semctl(sem_id, -1, IPC_RMID);
    myassert(ret != -1, "Le sémaphore ne s'est pas correctement détruit");
}

//============ UTILISATION DES SEMAPHORES ============

void operation_semaphore(int sem_id, int operation)
{
    // On effectue l'opération sur le sémaphore qui a pour id le paramètre de la fonction
    struct sembuf augmente = {0, operation, 0};
    int ret = semop(sem_id, &augmente, 1);
    myassert(ret != -1, "L'opération sur le sémaphore s'est mal déroulé");
}