#ifndef CLIENT_CRIBLE
#define CLIENT_CRIBLE

// On peut mettre ici des éléments propres au couple master/client :
//    - des constantes pour rendre plus lisible les comunications
//    - des fonctions communes (création tubes, écriture dans un tube,
//      manipulation de sémaphores, ...)

// ordres possibles pour le master
#define ORDER_NONE                0
#define ORDER_STOP               -1
#define ORDER_COMPUTE_PRIME       1
#define ORDER_HOW_MANY_PRIME      2
#define ORDER_HIGHEST_PRIME       3
#define ORDER_COMPUTE_PRIME_LOCAL 4   // ne concerne pas le master

/***********************
 * Pour les tubes nommés
 ***********************/

/****** Constantes *******/

// Nombres de tubes nommés utilisés
#define NB_NAMED_PIPES 2

// Indice du tableau de tubes nommés dans la structure du master
    // Indice pour le tube nommé du client vers le master
#define PIPE_CLIENT_MASTER 0
    // Indice pour le tube nommé du master vers le client
#define PIPE_MASTER_CLIENT 1

// Nom des tubes
    // Nom du tube nommé du client vers le master
#define NAMED_PIPE_CLIENT_MASTER "client_to_master"
    // Nom du tube nommé du master vers le client
#define NAMED_PIPE_MASTER_CLIENT "master_to_client"

// Pour savoir si un nombre est premier
    // Le nombre n'est pas premier
#define NUMBER_NOT_PRIME 0
    // Le nombre est premier
#define NUMBER_IS_PRIME 1

/****** Fonctions *******/

// Fonctions qui créer les tubes nommés et qui renvoient leurs noms :
const char* create_named_pipe(const char* pipe_path_name);

// Fonctions qui ouvrent, en ecritue ou en lecture seul, un tube nommé passé en paramètre
int open_named_pipe(const char* pipe, int flag);

// Fonction qui ferme un tube passé en paramètre
void close_named_pipe(int fd);

// Fonction qui détruit un tube nommé dont le nom est donné en paramètre
void destroy_named_pipe(const char* name);

// Fonctions général d'envoie et réception de données par un tube nommé
void send_data(int fd, int data);
int received_data(int fd);
// Envoie du nombre de calcul au client qui le réceptionne
void client_how_many(int fd);
// Envoie du plus grand nombre premier au client qui le réceptionne
void client_highest_prime(int fd);
// Envoie de l'accusé de reception au client qui le réceptionne
void client_stop(int fd);
// Envoie du nombre premier à tester au master qui le réceptionne
void client_is_prime(int fd, int number_tested);


/***********************
 * Pour les sémaphores
 ***********************/

#include <sys/types.h> // key_t
#include <sys/ipc.h> // key_t 

/****** Constantes *******/

// Nombres de Sémaphores utilisé
#define NB_SEMAPHORE 2

// Indices du tableau de Sémaphore dans la structure du master
    // Indice pour le sémaphore entre les clients
#define SEM_CLIENTS 0
    // Indice pour le sémaphore entre le master et un client
#define SEM_MASTER_CLIENT 1

// Fichier choisi pour l'identification des sémaphores
#define SEM_FICHIER "master_client.h"

// Identifiants pour le deuxième paramètre de ftok
    // Identifiant pour le sémaphore entre les clients eux-mêmes
#define SEM_ID_CLIENTS 1
    // Identifiant pour le sémaphore entre le master et un client
#define SEM_ID_MASTER_CLIENT 2

// Valeur des opérations sur les sémaphores
    // Augmentation d'un sémaphore
#define SEM_OP_INC +1
    // Diminution d'un sémaphore
#define SEM_OP_DEC -1


/****** Fonctions *******/

// Fonction pour génerer les clé des sémaphores
key_t get_key_semaphore(int sem_id_num);

// Fonction pour créer et initialiser des sémaphores
int creation_semaphore(key_t key, int semValInit);

// Fonction pour récupérer des sémaphores existant
int get_id_semaphore(key_t key);

// Détruit le sémaphore dont l'identifiant a été passé en paramètre
void destroy_semaphore(int sem_id);

// augmenter ou diminuer les sémaphores
void operation_semaphore(int sem_id, int operation);

#endif